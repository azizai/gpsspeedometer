package aziz.ai.gpsspeedometer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

import aziz.ai.gpsspeedometer.dao.GpsSpeedometerDao;
import aziz.ai.gpsspeedometer.model.TestingModel;
import aziz.ai.gpsspeedometer.model.TestingModelAdapter;

public class TestingListActivity extends AppCompatActivity {

    private RecyclerView view;
    private TestingModelAdapter adapter;
    private GpsSpeedometerDao dao;
    private ArrayList<TestingModel> testingRecords;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dao = new GpsSpeedometerDao(this);
        setContentView(R.layout.activity_testing_list);

        view = findViewById(R.id.recyclerview);
        view.setHasFixedSize(true);
        view.setLayoutManager(new LinearLayoutManager(this));
        testingRecords = testingRecordList();
        adapter = new TestingModelAdapter(this, testingRecords);
        view.setAdapter(adapter);

        SwipeGesture sg = new SwipeGesture(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);

        ItemTouchHelper touchHelper = new ItemTouchHelper(sg);
        touchHelper.attachToRecyclerView(view);

    }

    private ArrayList<TestingModel> testingRecordList(){

        return dao.getTestingValue();
    }

    class SwipeGesture extends ItemTouchHelper.SimpleCallback{

        public SwipeGesture(int dragDirs, int swipeDirs) {
            super(dragDirs, swipeDirs);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int id = viewHolder.getAdapterPosition();
            switch(direction){
                case ItemTouchHelper.LEFT:
                    TestingModel model = testingRecords.get(id);
                    if(model!= null && dao.deleteTestingValue(model.getId())){
                        testingRecords.remove(id);
                        adapter.notifyItemRemoved(id);
                    }
                    break;
                case ItemTouchHelper.RIGHT:
                    break;

            }
        }
    }
}