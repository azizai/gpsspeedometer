package aziz.ai.gpsspeedometer.model;

import java.util.ArrayList;

/**
 * Name: Abdul Aziz
 * SRN: 14004066
 */
public class TestingModel {

    private int id;
    private ArrayList<String> speed;
    private String averageSpeed;

    public TestingModel() {
    }

    public TestingModel(int id, ArrayList<String> speed, String averageSpeed) {
        this.id = id;
        this.speed = speed;
        this.averageSpeed = averageSpeed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<String> getSpeed() {
        return speed;
    }

    public void setSpeed(ArrayList<String> speed) {
        this.speed = speed;
    }

    public String getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(String averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public String getSpeedStr(){
         return speed.toString().replaceAll("\\[|\\]", "");
    }
}
