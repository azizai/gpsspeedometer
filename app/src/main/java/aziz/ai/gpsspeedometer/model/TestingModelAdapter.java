package aziz.ai.gpsspeedometer.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import aziz.ai.gpsspeedometer.MainActivity;
import aziz.ai.gpsspeedometer.R;


public class TestingModelAdapter extends RecyclerView.Adapter<TestingModelAdapter.TestingModelViewHolder> {

    Context context;
    ArrayList<TestingModel> testingModelList;

    public TestingModelAdapter(Context context, ArrayList<TestingModel> testingModelList) {
        this.testingModelList = testingModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public TestingModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TestingModelViewHolder(LayoutInflater.from(context).inflate(R.layout.test_model__view_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TestingModelViewHolder holder, int position) {

        if (testingModelList != null && testingModelList.size() > 0) {

                TestingModel testingModel = testingModelList.get(position);
                holder.idCol.setText(String.valueOf(testingModel.getId()));
                holder.SpeedCol.setText(testingModel.getSpeedStr());
                holder.averageSpeedCol.setText(testingModel.getAverageSpeed());
        } else {
            return;
        }
    }

    @Override
    public int getItemCount() {
        return testingModelList.size();
    }

    public class TestingModelViewHolder extends RecyclerView.ViewHolder {

        TextView idCol, SpeedCol, averageSpeedCol;

        public TestingModelViewHolder(@NonNull View itemView) {
            super(itemView);
            idCol = itemView.findViewById(R.id.id_cl);
            SpeedCol = itemView.findViewById(R.id.speed_list_cl);
            averageSpeedCol = itemView.findViewById(R.id.average_speed_cl);
        }
    }
}
