package aziz.ai.gpsspeedometer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import aziz.ai.gpsspeedometer.dao.GpsSpeedometerDao;
import aziz.ai.gpsspeedometer.model.TestingModel;
public class MainActivity extends AppCompatActivity {

    private Switch swLocaton;
    private Switch gpsMethodSw;
    private TextView milesValue;
    private TextView status;
    private Intent dataHistoryListIntent;
    private Executor ex;
    private UpdateUI th;
    private DecimalFormat df = new DecimalFormat("0.0");
    private LocationCallback locationCallBack;
    private Button averageButton;
    private EditText averageMph;
    private Button openActivityButton;
    //Google play services API that retrieves client location and speed
    private FusedLocationProviderClient flpc;
    //location and speed API configuration
    private LocationRequest lr;

    private static final int PERMISSION_FINE_LOCATION = 99;
    private static final double MPH_TO_KPH = 1.60934;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setGpsMethodSw(findViewById(R.id.gpsmethod));
        setMilesValue(findViewById(R.id.milesvalue));
        setStatus(findViewById(R.id.status));
        setSwLocaton(findViewById(R.id.sw_locationsupdates));
        setAverageButton(findViewById(R.id.average));
        setAverageMph(findViewById(R.id.averagemiles));
        openActivityButton = findViewById(R.id.activitybutton);

        // sets location request API configuration
        //set an interval of 500 milliseconds or 0.5 seconds between each request
        //set location request accuracy to high priority
        setLr(LocationRequest.create()
                .setInterval(500)
                .setFastestInterval(300)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(500));
        //https://developer.android.com/training/location/request-updates
        //Location CallBack
        setLocationCallBack(new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                updateSpeed(locationResult.getLastLocation(), "lcb");
            }
        });

        //Type of Sensor. GPS/Tower Cell
        getGpsMethodSw().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getGpsMethodSw().isChecked()) {
                    getLr().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    getStatus().setText("gps sensor");
                } else {
                    getLr().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                    getStatus().setText("wifi sensor");
                }
            }
        });

        addListenerToUpdateGPS();

        //turn off location service
        getSwLocaton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getSwLocaton().isChecked()) {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    }
                    getFlpc().requestLocationUpdates(getLr(), getLocationCallBack(), null);
                    addListenerToUpdateGPS();
                } else {
                    getMilesValue().setText("0");
                    getAverageMph().setText("");
                    status.setText("off");
                    flpc.removeLocationUpdates(locationCallBack);
                }
            }
        });

        //calculate average 10s button click listener
        getAverageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                th = new UpdateUI();
                ex = Executors.newSingleThreadExecutor();
                ex.execute(th);
            }

        });

        //opens testing history intent screen
        openActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dataHistoryListIntent == null) {
                    dataHistoryListIntent = new Intent(MainActivity.this, TestingListActivity.class);
                }
                startActivity(dataHistoryListIntent);
            }
        });

        getSwLocaton().performClick();
        getSwLocaton().performClick();

    }

    //Update Speed Text View
    private void updateSpeed(Location location, String source) {
        if (location.hasSpeed()) {
            float speed = location.getSpeed();
            getMilesValue().setText(String.format("%.0f", (2.236936 * speed)));
            getStatus().setText("read" + source);
        } else {
            getMilesValue().setText("0");
            getStatus().setText(source);
        }
    }

    private void addListenerToUpdateGPS() {
        setFlpc(LocationServices.getFusedLocationProviderClient(MainActivity.this));


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            getFlpc().getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // contian lat, long, speed etc
                    updateSpeed(location, "ocl");
                }
            });
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION);
            }
        }
    }

    //if app has no permission, this gets called
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addListenerToUpdateGPS();
                } else {
                    Toast.makeText(this, "need permission to work", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private int doubleToInt(Double d) {
        return (int) Math.round(d);
    }

    private boolean isNum(String d) {
        if (d == null) {
            return false;
        }
        try {
            Double.parseDouble(d);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private String getAverage(List<Integer> values) {
        if (values == null || values.isEmpty()) {
            return "null";
        }

        Double sum = 0.0;
        for (Integer value : values) {
            sum += value;
        }

        return String.valueOf(df.format(sum / values.size()));
    }

    public Switch getSwLocaton() {
        return swLocaton;
    }

    public void setSwLocaton(Switch swLocaton) {
        this.swLocaton = swLocaton;
    }

    public Switch getGpsMethodSw() {
        return gpsMethodSw;
    }

    public void setGpsMethodSw(Switch gpsMethodSw) {
        this.gpsMethodSw = gpsMethodSw;
    }

    public TextView getMilesValue() {
        return milesValue;
    }

    public void setMilesValue(TextView milesValue) {
        this.milesValue = milesValue;
    }

    public TextView getStatus() {
        return status;
    }

    public void setStatus(TextView status) {
        this.status = status;
    }

    public LocationCallback getLocationCallBack() {
        return locationCallBack;
    }

    public void setLocationCallBack(LocationCallback locationCallBack) {
        this.locationCallBack = locationCallBack;
    }

    public Button getAverageButton() {
        return averageButton;
    }

    public void setAverageButton(Button averageButton) {
        this.averageButton = averageButton;
    }

    public EditText getAverageMph() {
        return averageMph;
    }

    public void setAverageMph(EditText averageMph) {
        this.averageMph = averageMph;
    }

    public FusedLocationProviderClient getFlpc() {
        return flpc;
    }

    public void setFlpc(FusedLocationProviderClient flpc) {
        this.flpc = flpc;
    }

    public LocationRequest getLr() {
        return lr;
    }

    public void setLr(LocationRequest lr) {
        this.lr = lr;
    }

    private class UpdateUI extends Thread implements Runnable {

        int x = 0;
        GpsSpeedometerDao dao;


        @Override
        public void run() {
            dao = new GpsSpeedometerDao(MainActivity.this);

            ArrayList<Integer> milesList = new ArrayList<>();
//            ArrayList<Integer> kmList = new ArrayList<>();

            while (x < 10) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        EditText averageMiles = findViewById(R.id.averagemiles);

                        if (x == 0) {
                            averageMiles.setText("mph: ");
                        }
                        TextView mv = findViewById(R.id.milesvalue);
                        EditText am = findViewById(R.id.averagemiles);
                        String mile = mv.getText().toString();

                        if (!isNum(mile)) {
//                            x++;
                            return;
                        }
                        Integer mph = Integer.valueOf(mile);

                        milesList.add(mph);

                        am.append(String.valueOf(mph));

                        x++;


                        if (x == 10) {
                            String averageMile = getAverage(milesList);
                            averageMiles.append("\nAverage mph: " + averageMile);
                            dao.addTestingValue(new TestingModel(-1, getArrayString(milesList), averageMile));
                            return;
                        } else {
                            averageMiles.append(", ");
                        }


                    }
                });

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ArrayList<String> getArrayString(ArrayList<Integer> values){
        ArrayList<String> tmp = new ArrayList<String>();
        for (Integer i: values) {
            tmp.add(String.valueOf(i));
        }
        return tmp;
    }


}