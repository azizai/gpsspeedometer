package aziz.ai.gpsspeedometer.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;

import aziz.ai.gpsspeedometer.model.TestingModel;

public class GpsSpeedometerDao extends SQLiteOpenHelper {


    public static final String TESTING_TB = "TESTING_TB";
    public static final String SPEED = "SPEED";
    public static final String AVERAGE_SPEED = "AVERAGE_" + SPEED;

    public GpsSpeedometerDao(@Nullable Context context) {
        super(context, "testing.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TESTING_TB + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + SPEED + " TEXT, " + AVERAGE_SPEED + " TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    
    public boolean addTestingValue(TestingModel tm){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SPEED, tm.getSpeedStr());
        cv.put(AVERAGE_SPEED, tm.getAverageSpeed());

        long result = db.insert(TESTING_TB, null, cv);
        if(result > 0){
            db.close();
            return true;
        }
        db.close();
        return false;
    }

    public boolean deleteTestingValue(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        int xx = db.delete(TESTING_TB, "ID = ?", new String[]{String.valueOf(id)});
        if ((xx) > 0){
            db.close();
            return true;
        }
        db.close();
        return false;

    }

    public ArrayList<TestingModel> getTestingValue(){

        String selectTesting = "SELECT * from " + TESTING_TB;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectTesting, null);

        ArrayList<TestingModel> testingModels = new ArrayList<>();

        while(cursor.moveToNext()){

            int id = cursor.getInt(0);
            String speed = cursor.getString(1);
            String averageSpeed = cursor.getString(2);
            testingModels.add(new TestingModel(id, new ArrayList<String>(Arrays.asList(speed.split(","))), averageSpeed));
        }
        cursor.close();
        db.close();
        return testingModels;

    }
}
